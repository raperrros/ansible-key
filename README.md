---

Change vars in roles/git/defaults/main.yml

key_path: #Your key path, ex: "/root"

key_type: #Your key type, ex: "ed25519"

key_size: #Your key size, ex: '4096'

git_repository: #Your git repo, ex: "git@gitlab.com:raperrros/terraform-two-server.git"

path_to_clone_repository: #path to clone repo, ex: "/root/test"

private_key_name: #your private key name, ex: "bb.pem"

public_key_name: #your public key name, ex: "{{ private_key_name }}.pub"

hostname: #hostname git, ex: "gitlab.com" , maybe "bitbucket.org"

comment_for_public_key: #your comment for pablic key, ex: "root@cruise-control.space"

**Attention! Repository clone folder is not created, it must be created**
